<?php
    require 'gPoint.php';

    $numberLine = 1;
    $gpoint = new gPoint();
    $dataset = fopen('dataset_php.csv', 'r');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
        <style>
            html, body {
                height: 100vh;
                margin: 0;
                padding: 0;
            }

            #map {
                height: 100vh;
            }

            #buttons {
                position: absolute;
                right: 5px;
                top: 20vh;
                z-index: 1;
            }
        </style>
        <title>CDA - Trabajo</title>
    </head>
    <body>
        <div id="map"></div>
        <script>
            var map = null,
                lat = 39.4666667,
                long = -0.3666667;

            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: new google.maps.LatLng(lat, long),
                    scrollwheel: true,
                    zoom: 14
                });
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoqJonEDqL5ePNAjOIlkneI417v2dJlZY&signed_in=true&callback=initMap"></script>
        <div id="buttons">
            <button id="c1">1</button>
            <button id="c2">2</button>
            <button id="c3">3</button>
            <button id="c4">4</button>
            <button id="c5">5</button>
            <button id="c5">6</button>
            <button id="cAll">All</button>
        </div>
        <script type="application/javascript">
            var iconCluster = [];
            iconCluster[1] = 'http://maps.google.com/mapfiles/marker_black.png';
            iconCluster[2] = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
            iconCluster[3] = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
            iconCluster[4] = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
            iconCluster[5] = 'http://maps.google.com/mapfiles/ms/icons/pink-dot.png';
            iconCluster[6] = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';

            var clusters = [];
            clusters[1] = [];
            clusters[2] = [];
            clusters[3] = [];
            clusters[4] = [];
            clusters[5] = [];
            clusters[6] = [];

            var indexCluster = 0;

            function show(c) {
                for (var i = 1; i < clusters.length; i++) {
                    for (var j = 0; j < clusters[i].length; j++) {
                        clusters[i][j].setMap(null);
                    }
                }

                if (c == 0)
                    for (i = 1; i < clusters.length; i++) {
                        for (j = 0; j < clusters[i].length; j++) {
                            clusters[i][j].setMap(map);
                        }
                    }
                else
                    for (i = 0; i < clusters[c].length; i++) {
                        clusters[c][i].setMap(map);
                    }
            }

            $(document).ready(function () {
                /*var location = null;
                for (var i = 0; i < res.length; i++) {
                    location = new google.maps.LatLng(res[i]['lat'], res[i]['long']);
                    clusters[indexCluster][i] = new google.maps.Marker({
                        position: location,
                        map: map,
                        icon: iconCluster[indexCluster]
                    });
                }*/

                <?php
                while (($line = fgetcsv($dataset, 1000, ',')) !== FALSE) {
                    $cluster = intval($line[1]);
                    $gpoint->setUTM(floatval($line[2]), floatval($line[3]), '30N');
                    $gpoint->convertTMtoLL();
                    $lat = $gpoint->Lat();
                    $long = $gpoint->Long();
                    ?>
                    clusters[<?php echo $cluster; ?>].push(
                        new google.maps.Marker({
                            position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                            map: map,
                            icon: iconCluster[<?php echo $cluster; ?>]
                        })
                    );
                    <?php $numberLine++;
                }
                ?>

                $("#c1").click(function () {
                    show(1);
                });
                $("#c2").click(function () {
                    show(2);
                });
                $("#c3").click(function () {
                    show(3);
                });
                $("#c4").click(function () {
                    show(4);
                });
                $("#c5").click(function () {
                    show(5);
                });
                $("#c6").click(function () {
                    show(6);
                });
                $("#cAll").click(function () {
                    show(0);
                });
            });
        </script>
    </body>
</html>
<?php
    fclose($dataset);
?>