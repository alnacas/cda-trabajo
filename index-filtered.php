<?php
    require 'gPoint.php';

    function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }

    $clusters = array();
    $c_finals = array();
    $barrios = array();
    $gpoint = new gPoint();

    $dataset = fopen('dataset_php.csv', 'r');
    while (($line = fgetcsv($dataset, 1000, ',')) !== FALSE) {
        $cluster = intval($line[1]);
        $gpoint->setUTM(floatval($line[2]), floatval($line[3]), '30N');
        $gpoint->convertTMtoLL();
        $lat = $gpoint->Lat();
        $long = $gpoint->Long();
        $clusters[$cluster][] = array($line[0], $lat, $long);
    }
    for ($i = 1; $i <= count($clusters); $i++) {
        array_multisort($clusters[$i]);
        for ($j = 0; $j < count($clusters[$i]) - 1; $j++) {
            $distancia = distance($clusters[$i][$j][1], $clusters[$i][$j][2], $clusters[$i][$j + 1][1], $clusters[$i][$j + 1][2]);
            if ($distancia > 2) {
                $c_finals[$i][] = $clusters[$i][$j];
            }
        }
    }

    $dtbarrios = fopen('barrios.csv', 'r');
    while (($line = fgetcsv($dtbarrios, 20000, ';')) !== FALSE) {
        $aux = array();
        $ps = explode(',', $line[0]);
        for ($i = 0; $i < count($ps); $i++) {
            $co = explode(' ', $ps[$i]);
            $gpoint->setUTM(floatval($co[0]), floatval($co[1]), '30N');
            $gpoint->convertTMtoLL();
            $lat = $gpoint->Lat();
            $long = $gpoint->Long();
            $aux[] = array($lat, $long);
        }
        $barrios[] = array('nombre' => $line[2], 'coords' => $aux);
    }
    array_multisort($barrios);
    /*foreach ($barrios as $barrio) {
        echo '<h3>' . $barrio['nombre'] . '</h3>';
        foreach ($barrio['coords'] as $coord) {
            echo $coord[0] . ' ' . $coord[1] . '<br>';
        }
    }*/
?>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
        <style>
            html, body {
                height: 100vh;
                margin: 0;
                padding: 0;
            }

            #map {
                height: 100vh;
            }

            #buttons {
                position: absolute;
                right: 5px;
                top: 10vh;
                z-index: 1;
                text-align: right;
                height: 70vh;
                overflow-y: scroll;
            }

            .onMap {
                color: green;
                font-weight: bold;
            }
        </style>
        <title>CDA - Trabajo</title>
    </head>
    <body>
        <div id="buttons">
            <button id="todos">Ver todos</button><br>
            <button id="ninguno">Quitar todos</button><br>
            <?php $number = 0;
            foreach ($barrios as $barrio) { ?>
                <button id="barrio_<?php echo $number; ?>"><?php echo $barrio['nombre']; ?></button>
                <br>
            <?php $number++;
            } ?>
        </div>
        <div id="map"></div>
        <script>
            var map = null,
                lat = 39.4666667,
                long = -0.3666667,
                polygons = [],
                polygonsCluster = [],
                polygonsMap = [];

            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: new google.maps.LatLng(lat, long),
                    scrollwheel: true,
                    zoom: 14
                });

                <?php foreach ($barrios as $barrio) { ?>
                    polygons["<?php echo $barrio['nombre']; ?>"] = [];
                    polygonsCluster["<?php echo $barrio['nombre']; ?>"] = [];
                    polygonsMap["<?php echo $barrio['nombre']; ?>"] = false;
                    <?php foreach ($barrio['coords'] as $coord) { ?>
                        polygons["<?php echo $barrio['nombre']; ?>"].push({'lat':<?php echo $coord[0] ?>, 'lng':<?php echo $coord[1] ?>});
                    <?php } ?>
                    polygons["<?php echo $barrio['nombre']; ?>"] = new google.maps.Polygon({
                        paths: polygons["<?php echo $barrio['nombre']; ?>"],
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35
                    });
                <?php } ?>
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoqJonEDqL5ePNAjOIlkneI417v2dJlZY&signed_in=true&callback=initMap"></script>
        <script type="application/javascript">
            var iconCluster = [];
            iconCluster[1] = 'http://maps.google.com/mapfiles/marker_black.png';
            iconCluster[2] = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
            iconCluster[3] = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
            iconCluster[4] = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
            iconCluster[5] = 'http://maps.google.com/mapfiles/ms/icons/pink-dot.png';
            iconCluster[6] = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';

            var clusters = [];
            clusters[1] = [];
            clusters[2] = [];
            clusters[3] = [];
            clusters[4] = [];
            clusters[5] = [];
            clusters[6] = [];

            var position = null,
                marker = null;

            $(document).ready(function () {
                <?php
                for ($i = 1; $i <= count($c_finals); $i++) {
                    for ($j = 0; $j < count($c_finals[$i]); $j++) {
                        $lat = $c_finals[$i][$j][1];
                        $long = $c_finals[$i][$j][2]; ?>
                        position = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>);
                        marker = new google.maps.Marker({
                            position: position,
//                            map: map,
                            icon: iconCluster[<?php echo $i; ?>]
                        });
                        clusters[<?php echo $i; ?>].push(marker);
                        for (var polygon in polygons) {
                            if (google.maps.geometry.poly.containsLocation(position, polygons[polygon])) {
                                polygonsCluster[polygon].push(marker);
                            }
                        }
                    <?php }
                }
                $number = 0;
                foreach ($barrios as $barrio) { ?>
                    $("#barrio_<?php echo $number; ?>").click(function () {
                        if (polygonsMap["<?php echo $barrio['nombre']; ?>"]) {
                            for (var i = 0; i < polygonsCluster["<?php echo $barrio['nombre']; ?>"].length; i++) {
                                polygonsCluster["<?php echo $barrio['nombre']; ?>"][i].setMap(null);
                            }
                            polygonsMap["<?php echo $barrio['nombre']; ?>"] = false;
                            $(this).removeClass('onMap');
                        } else {
                            for (i = 0; i < polygonsCluster["<?php echo $barrio['nombre']; ?>"].length; i++) {
                                polygonsCluster["<?php echo $barrio['nombre']; ?>"][i].setMap(map);
                            }
                            polygonsMap["<?php echo $barrio['nombre']; ?>"] = true;
                            $(this).addClass('onMap');
                        }
                    });
                    <?php $number++;
                } ?>
                $("#todos").click(function () {
                    for (var i = 1; i < clusters.length; i++) {
                        for (var j = 0; j < clusters[i].length; j++) {
                            clusters[i][j].setMap(map);
                        }
                    }
                    $("button[id ^= barrio_]").each(function () {
                        $(this).addClass('onMap');
                    });
                });
                $("#ninguno").click(function () {
                    for (var i = 1; i < clusters.length; i++) {
                        for (var j = 0; j < clusters[i].length; j++) {
                            clusters[i][j].setMap(null);
                        }
                    }
                    $("button[id ^= barrio_]").each(function () {
                        $(this).removeClass('onMap');
                    });
                });
                if (confirm("Página lista, ¿ver todos los árboles?")) {
                    $("#todos").click();
                }
            });
        </script>
    </body>
</html>
<?php
    fclose($dataset);
    fclose($dtbarrios);
?>