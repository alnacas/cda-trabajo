<?php

require("gPoint.php");

// ###Main de la aplicacion###
if (!empty($_GET)) {
    switch ($_GET['type']) {
        case 'movred':
            $movred = descargaDatosDeWeb('http://apigobiernoabiertocatalog.valencia.es/api/3/action/package_show?id=aparcamiento-movilidad-reducida',
                $_GET['lat'], $_GET['long']);
            echo json_encode($movred);
            break;
        case 'motos':
            $motos = descargaDatosDeWeb('http://apigobiernoabiertocatalog.valencia.es/api/3/action/package_show?id=aparcamiento-para-motos',
                $_GET['lat'], $_GET['long']);
            echo json_encode($motos);
            break;
        case 'ora':
            $ora = descargaDatosDeWeb('http://apigobiernoabiertocatalog.valencia.es/api/3/action/package_show?id=aparcamientos-ora',
                $_GET['lat'], $_GET['long']);
            echo json_encode($ora);
            break;
        case 'npark':
            $npark = descargaDatosDeWeb('http://apigobiernoabiertocatalog.valencia.es/api/3/action/package_show?id=aparcamientos-no-regulados',
                $_GET['lat'], $_GET['long']);
            echo json_encode($npark);
            break;
        case 'ppark':
            $ppark = descargaDatosDeWeb('http://apigobiernoabiertocatalog.valencia.es/api/3/action/package_show?id=aparcamientos-vehiculos',
                $_GET['lat'], $_GET['long']);
            echo json_encode($ppark);
            break;
    }
}

// ###Funciones principales###
// Devuelve un array con las cordenadas en formato Lat/Long del dataset señalado en la URL.
function descargaDatosDeWeb($url, $latFrom, $longFrom)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'cURL Request'
    ));

    $resp = curl_exec($curl);

    $rp1 = json_decode($resp, TRUE);

    $url2 = $rp1["result"]["resources"][2]["url"];

    curl_close($curl);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url2,
        CURLOPT_USERAGENT => 'cURL Request'
    ));

    $resp = curl_exec($curl);
    $resp = json_decode($resp, true);

    curl_close($curl);

    $datos = $resp["features"];

    $coords = array();
    $gpoint = new gPoint();

    for ($i = 0; $i < count($datos); $i++) {
        $coord = $datos[$i]["geometry"]["coordinates"];

        if (count($coord[0]) > 1) {
            $coord = $datos[$i]["geometry"]["coordinates"][0];
        }

        $gpoint->setUTM($coord[0], $coord[1], '30N');
        $gpoint->convertTMtoLL();

        $dist = distance($latFrom, $longFrom, $gpoint->Lat(), $gpoint->Long());

        if ($dist <= 0.4) {

            $coords[] = array("lat" => $gpoint->Lat(), "long" => $gpoint->Long(), "dist" => $dist);
        }
    }

    return $coords;
}

function distance($lat1, $lon1, $lat2, $lon2)
{
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;

    return ($miles * 1.609344);
}